require_relative 'db/connection'
require_relative 'lib/api_connection'
require_relative 'lib/movie'
require_relative 'lib/actor'
require_relative 'lib/director'
require_relative 'lib/category'
require_relative 'lib/view_builder'
require 'rest-client'
require 'json'

system('clear')

class Menu

  attr_accessor :title, :movie, :result, :choice, :movies

  def self.display
    menu = "1. See all movies available in database \n\n"
    menu += "2. Search by title \n\n"
    print menu
  end

  def self.get_user_input
    loop do
      print "Your choice: "
      @choice = gets.chomp.to_i
      break if [1,2].include?( @choice )
      print "Invalid choice. Please try again\n"
    end
  end

  def self.sort
    case @choice
    when 1
      @movies = Movie.all
      ViewBuilder.generate_multi @movies
      print "Your HTML is ready for browser viewing\n"
    when 2
      title_search
    end
  end

  def self.title_search
    get_title
    db_find_by_title
    if @result.empty?
      search_in_db
    end
    generate_single_movie_view
  end

  def self.search_in_db
    print "Your movie does not exist in our database. Press enter to go ahead and include it "; gets
    api_find_by_title
    @movie = fill_db
  end

  def self.generate_single_movie_view
    print "Generating HTML view...\n"
    ViewBuilder.generate_single @movie
    print "Your HTML is ready for browser viewing\n"
  end

  def self.get_title
    print "Title to search: "
    @title = gets.chomp
  end

  def self.db_find_by_title
    @result = Movie.where("title LIKE ?", "%#{ @title }%" )
    unless @result.empty? then @movie = @result[0] end
  end

  def self.api_find_by_title
    ApiConnector.set_url( @title )
    ApiConnector.get_response
    ApiConnector.parse_to_json
  end

  def self.fill_db
    ApiConnector.to_db
  end

  def self.execute
    display
    get_user_input
    sort
  end
end

Menu.execute
