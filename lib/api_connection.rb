class ApiConnector < ActiveRecord::Base

  attr_accessor :url, :response, :json

  def self.set_url( title )
    @url = "http://www.omdbapi.com/?apikey=dbe7d26f&t=#{ title }"
  end

  def self.get_response
    @response = RestClient.get( @url )
  end

  def self.parse_to_json
    @json = JSON.parse( @response.body )
  end

  def self.to_db
    movie = Movie.create({
      title:  @json['Title'],
      year:  @json['Year'],
      rated:  @json['Rated'],
      language:  @json['Language'],
      poster:  @json['Poster'],
      plot:  @json['Plot']
    })

    categories = @json['Genre']
    categories.split(",").each do |item|
      category = Category.where("name = ?", item)
      if category.empty?
        category = Category.create({
          name: item
        })
      end
      movie.categories << category
    end

    actors = @json['Actors']
    actors.split(",").each do |item|
      actor = Actor.where("name = ?", item)
      if actor.empty?
        actor = Actor.create({
          name: item
        })
      end
      movie.actors << actor
    end

    director = Director.where("name = ?", @json['Director'])
    if director.empty?
      director = Director.create({
        name: @json['Director']
      })
    end
    movie.directors << director

    @movie = movie
    @movie
  end

end
