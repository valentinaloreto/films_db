class ViewBuilder
  attr_accessor :multi

  def self.generate_single movie
    actors = movie.actors.map(&:name).join(", ")
    genres = movie.categories.map(&:name).join(", ")
    directors = movie.directors.map(&:name).join(", ")

    data = File.read("/home/valentina/Desktop/movies/views/view.html")

    data = data.sub("@title", "#{ movie['title'] }")
    data = data.sub("@year", "#{ movie['year'] }")
    data = data.sub("@category", "#{ genres }")
    data = data.sub("@language", "#{ movie['language'] }")
    data = data.sub("@director", "#{ directors }" )
    data = data.sub("@actors", "#{ actors }")
    data = data.sub("@poster", "#{ movie['poster'] }")
    data = data.sub("@plot", "#{ movie['plot'] }")

    fileHtml = File.new("/home/valentina/Desktop/movies/views/custom_view.html", "w+")
    fileHtml.puts data
    fileHtml.close()
  end






  def self.generate_multi movies

    @multi = ""

    data = File.read("/home/valentina/Desktop/movies/views/view_multi.html")

    beginning = data.split("<!--@beginning@-->")[0]
    ending = data.split("<!--@end@-->")[1]
    aux = data.split("<!--@beginning@-->")[1]
    middle = aux.split("<!--@end@-->")[0]



    movies.each do | movie |

      middle = aux.split("<!--@end@-->")[0]

      actors = movie.actors.map(&:name).join(", ")
      genres = movie.categories.map(&:name).join(", ")
      directors = movie.directors.map(&:name).join(", ")

      middle = middle.sub("@title", "#{ movie['title'] }")
      middle = middle.sub("@year", "#{ movie['year'] }")
      middle = middle.sub("@category", "#{ genres }")
      middle = middle.sub("@language", "#{ movie['language'] }")
      middle = middle.sub("@director", "#{ directors }" )
      middle = middle.sub("@actors", "#{ actors }")
      middle = middle.sub("@poster", "#{ movie['poster'] }")
      middle = middle.sub("@plot", "#{ movie['plot'] }")



      @multi += middle


    end


    fileHtml = File.new("/home/valentina/Desktop/movies/views/custom_view.html", "w+")
    fileHtml.puts beginning + @multi + ending
    fileHtml.close()
  end

end

