class CreateDirectorsMovies < ActiveRecord::Migration[5.2]
  def change
    create_table :directors_movies do |t|
      t.references :director, foreign_key: true
      t.references :movie, foreign_key: true
    end
  end
end
